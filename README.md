# Pi with stuff
Install a bunch of stuff on a Raspberry pi for home use.

Most of this is stolen from Mr Ansible (https://github.com/geerlingguy/internet-pi)[https://github.com/geerlingguy/internet-pi]

## Setup
Copy `config.example.py` and `inventory.example.ini` to files without example in them, and fill them out
with your own values

Clone your sd-card, with user `pi` and a password of choice

Boot it up, update it and install ansible
```
sudo apt install -y python3-pip
pip3 install ansible
```

Copy your ssh key over to the pi so you don't need to write the password
```
ssh-copy-id -i ~/.ssh/id_rsa pi@XXX.XXX.XXX.XXX
```

Run the notebook and wait, because of the docker group permissions you might need to 
run it twice before it works.

## Pi-hole
If you get the error `host is already in use`, make sure that nothing is running 
```
$sudo lsof -i -P -n
```
Use the command above to see what is running (if your using Debian/Ubuntu, it's probably systemd-r)

### Turning off systemd-r 
edit `/etc/systemd/resolved.conf` and uncomment and add the line looking like:
```
DNSStubListener=no
```
Then restart with `service systemd-resolved restart`

## Home assistant
Special container, needs to run with privileged rights. Include the following to 
make it work behind reverse proxy (in /config/configuration.yml)
```
http:
  use_x_forwarded_for: true
    trusted_proxies:
        - 172.19.0.0/24
```

## VictoriaMetrics / Prometheus
You need to create an LongLiveToken (click user profile in Home Assistant and then at the bottom there is a user profile)
that is used when calling the Prometheus api in home assistant

## Grafana
Has annoying permission properties... 
Run `sudo chmod 777 grafana-storage` and it should work fine, still need to find a solution

## Using external drive
Make sure it's formatted with ext4 and clean, if you need to use it for more stuff
then adjust folder accordningly.

First plug in drive and find it with `lsusb` to see that it was connected and
note down the name of it (you will find it as `/dev/<name>`)

Mount the disk wherever you want (I'm using `/mnt/external_drive`)

```
sudo mkdir /mnt/external_drive
sudo mount /dev/sda1 /mnt/external_drive
```
Check that it works with `lsblk` and check the mounting point for your drive

Make it permanent by copying the UUID string and add that to default
```
ls -l /dev/disk/by-uuid/
```
Find the one pointing toward your mounting point and copy the UUID, add that to fstab file (`nano /etc/fstab`)
```
UUID=3947b55c-fd1d-4d50-99b6-fb4189c75344 /mnt/external_drive ext4 defaults 0 0
```

Now you need to copy the current config (if any) there and then repoint config dir to your new location
```
sudo cp -ar /home/pi/ /mnt/external_drive/
```
